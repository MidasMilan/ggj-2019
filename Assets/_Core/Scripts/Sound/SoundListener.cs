﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SoundListener : MonoBehaviour
{
    public const float SOUND_DECAY_INTERVAL_SECONDS = 0.25f;
    public const float SOUND_DISTANCE_WEIGHT = 0.25f;

    private static SoundEmitter[] FindAllEmitters()
    {
        return FindObjectsOfType<SoundEmitter>();
    }

    public delegate void ListenerAwokenEventHandler(SoundListener listener);
    public event ListenerAwokenEventHandler ListenerAwokenEvent;

    [SerializeField] private float soundDecay; //How fast the meter goes down
    [SerializeField] private float soundRequiredForWake; //sound meter target

    private float soundHearing; //sound meter current value
    private bool isHearingEnoughSoundForWake { get { return soundHearing >= soundRequiredForWake; } }
    private float nextDecayTargetTimestamp { get { return lastDecayTimestamp + SOUND_DECAY_INTERVAL_SECONDS; } }
    private float lastDecayTimestamp;

    public void ResetListener()
    {
        soundHearing = 0f;
        enabled = true;
    }

    private void OnEnable()
    {
        SoundEmitter[] emitters = FindAllEmitters();
        for (int i = 0; i < emitters.Length; i++)
        {
            if(emitters[i].gameObject != gameObject)
            {
                emitters[i].SoundEmittedEvent += OnSoundEmitted;
            }
        }
    }

    private void OnDisable()
    {
        SoundEmitter[] emitters = FindAllEmitters();
        for (int i = 0; i < emitters.Length; i++)
        {
            if (emitters[i].gameObject != gameObject)
            {
                emitters[i].SoundEmittedEvent -= OnSoundEmitted;
            }
        }
    }

    private void Update()
    {
        if (soundHearing > 0f && Time.realtimeSinceStartup >= nextDecayTargetTimestamp)
        {
            soundHearing = Mathf.Clamp(soundHearing - soundDecay, 0f, soundRequiredForWake);
            lastDecayTimestamp = Time.realtimeSinceStartup;
        }
    }

    private void OnDrawGizmos()
    {
        Handles.Label(transform.position, "Hearing: " + soundHearing + " / " + soundRequiredForWake);
    }

    private void OnSoundEmitted(Vector3 position, float volume)
    {
        float distance = Vector3.Distance(transform.position, position) * SOUND_DISTANCE_WEIGHT;
        float volumeHeard = volume / distance;

        soundHearing += volumeHeard;

        if(isHearingEnoughSoundForWake)
        {
            ListenerAwokenEvent?.Invoke(this);
            enabled = false;
        }
    }
}
