﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SoundWaves : MonoBehaviour
{
    public const int MAX_RINGS = 1;
    public const float RING_TWEEN_INTERVAL = 0.25f;
    public const float RING_TWEEN_MIN_DURATION = 0.65f;
    public const float RING_TWEEN_MAX_DURATION = 1.15f;
    public const float RING_RED_THRESHOLD = 0.75f;
    public const Ease RING_TWEEN_EASE = Ease.Linear;
    public readonly Vector2 RING_MAX_SCALE = new Vector2(1.75f, 1.75f);

    private SoundEmitter soundEmitter;
    private List<SpriteRenderer> soundRings;

    private void Awake()
    {
        soundEmitter = GetComponent<SoundEmitter>();
        soundRings = new List<SpriteRenderer>();
    }

    private void OnEnable()
    {
        soundEmitter.SoundEmittedEvent += OnSoundEmitted;
    }

    private void OnDisable()
    {
        soundEmitter.SoundEmittedEvent -= OnSoundEmitted;
    }

    private void OnSoundEmitted(Vector3 position, float volume)
    {
        float normalizedVolume = volume / SoundEmitter.MAX_SOUND_EMIT_VOLUME;
        Color color = normalizedVolume >= RING_RED_THRESHOLD ? Color.red * 0.75f : Color.white;
        int rings = Mathf.Clamp((int)(normalizedVolume * MAX_RINGS), 1, MAX_RINGS);
        Vector2 scale = normalizedVolume * RING_MAX_SCALE * UnityEngine.Random.Range(0.95f, 1.05f);
        float tweenDuration = RING_TWEEN_MIN_DURATION + ((RING_TWEEN_MAX_DURATION - RING_TWEEN_MIN_DURATION) * normalizedVolume);
        
        SpriteRenderer ring = FindAvailableRing();
        ring.gameObject.SetActive(true);

        //Reset values
        ring.transform.localScale = Vector3.zero;
        ring.transform.position = position;
        ring.color = Color.clear;
        ring.DOKill();

        //Start tweens
        ring.transform.DOScale(scale, tweenDuration).SetEase(RING_TWEEN_EASE);
        ring.DOColor(color, tweenDuration * 0.75f).OnComplete(delegate ()
        {
            ring.DOColor(Color.clear, tweenDuration * 0.5f).OnComplete(delegate ()
            {
                ring.gameObject.SetActive(false);
            });
        }).SetEase(RING_TWEEN_EASE);
    }

    private SpriteRenderer SpawnRing()
    {
        GameObject prefab = Resources.Load("FX/Sound Wave Ring") as GameObject;
        GameObject ring = Instantiate(prefab);
        ring.SetActive(false);
        soundRings.Add(ring.GetComponent<SpriteRenderer>());

        return soundRings[soundRings.Count - 1];
    }

    private SpriteRenderer FindAvailableRing()
    {
        for (int i = 0; i < soundRings.Count; i++)
        {
            if (!DOTween.IsTweening(soundRings[i]))
                return soundRings[i];
        }
        return SpawnRing();
    }
}