﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SoundWaves))]
public class SoundEmitter : MonoBehaviour
{
    public const float MAX_SOUND_EMIT_VOLUME = 10f;

    public delegate void SoundEmittedEventHandler(Vector3 position, float volume);
    public event SoundEmittedEventHandler SoundEmittedEvent;

    [Range(0, MAX_SOUND_EMIT_VOLUME)]
    [SerializeField] public float volume;

    public void EmitSound()
    {
        SoundEmittedEvent?.Invoke(transform.position, volume);
    }
    public void EmitSound(Vector3 position)
    {
        SoundEmittedEvent?.Invoke(position, volume);
    }
}
