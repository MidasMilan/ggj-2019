﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bed : MonoBehaviour
{
    [SerializeField] private int scoreValue;

    private Interactable interactable;

    private void OnEnable()
    {
        if(interactable == null) { interactable = GetComponent<Interactable>(); }

        interactable.InteractEvent += OnInteract;
    }

    private void OnDisable()
    {
        interactable.InteractEvent -= OnInteract;
    }

    private void OnInteract(PlayerInteracting player, Interactable interactable)
    {
        ScoreCounter score = FindObjectOfType<ScoreCounter>();
        score.AddUpPoints(scoreValue);
        score.TallyBriefingPoints();

        player.GetComponent<PlayerMovement>().enabled = false;
        TimeController.PauseTimeEvent?.Invoke();

        interactable.enabled = false;
    }
}
