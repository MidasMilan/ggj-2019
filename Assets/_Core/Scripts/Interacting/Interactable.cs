﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D), typeof(InteractableVisuals))]
public class Interactable : MonoBehaviour
{
    public delegate void InteractEventHandler(PlayerInteracting player, Interactable interactable);
    public delegate void InteractInRangeStateChangedEventHandler(bool newState);
    public event InteractEventHandler InteractEvent;
    public event InteractInRangeStateChangedEventHandler InteractInRangeStateChangedEvent;

    private void OnDisable()
    {
        SetInRange(null, false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerInteracting player = collision.GetComponent<PlayerInteracting>();
        player?.AddInteractableInRange(this);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerInteracting player = collision.GetComponent<PlayerInteracting>();
        player?.RemoveInteractableInRange(this);
    }

    public void SetInRange(PlayerInteracting player, bool state)
    {
        InteractInRangeStateChangedEvent?.Invoke(state);

        if(player == null) { return; }
        if (state)
        {
            player.InteractEvent += OnInteract;
        }
        else
        {
            player.InteractEvent -= OnInteract;
        }
    }

    private void OnInteract(PlayerInteracting player, Interactable interactable)
    {
        InteractEvent?.Invoke(player, interactable);
    }
}