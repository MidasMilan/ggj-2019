﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteracting : MonoBehaviour
{
    public event Interactable.InteractEventHandler InteractEvent;

    private List<Interactable> interactablesInRange = new List<Interactable>();
    private Interactable currentClosestInteractable;

    public void AddInteractableInRange(Interactable interactable)
    {
        interactablesInRange.Add(interactable);
    }

    public void RemoveInteractableInRange(Interactable interactable)
    {
        interactablesInRange.Remove(interactable);
    }

    private void Update()
    {
        Interactable closestInteractable = GetClosestInteractable();
        if(closestInteractable == null)
        {
            currentClosestInteractable?.SetInRange(this, false);
            currentClosestInteractable = null;
            return;
        }
        
        if(closestInteractable != currentClosestInteractable)
        {
            currentClosestInteractable?.SetInRange(this, false);
            currentClosestInteractable = closestInteractable;
            currentClosestInteractable.SetInRange(this, true);
        }

        if(Input.GetKeyUp(KeyCode.Space))
        {
            InteractEvent?.Invoke(this, closestInteractable);
        }
    }

    private Interactable GetClosestInteractable()
    {
        float bestDist = float.MaxValue;
        Interactable bestInt = null;

        for (int i = interactablesInRange.Count - 1; i >= 0; i--)
        {
            Interactable interactable = interactablesInRange[i];

            if(!interactable.enabled)
            {
                RemoveInteractableInRange(interactable);
                continue;
            }

            float distance = Vector2.Distance(transform.position, interactable.transform.position);

            if(distance < bestDist)
            {
                bestDist = distance;
                bestInt = interactable;
            }
        }
        return bestInt;
    }
}