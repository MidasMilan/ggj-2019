﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InteractableVisuals : MonoBehaviour
{
    public const float IN_RANGE_TWEEN_DURATION = 0.65f;

    [SerializeField] private Sprite standardSprite;
    [SerializeField] private Sprite inRangeSprite;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Ease inRangeTweenEase;

    private Interactable interactable;
    private SpriteRenderer inRangeSpriteRenderer;
    private Sequence fadeSequence;

    private void Awake()
    {
        interactable = GetComponent<Interactable>();

        inRangeSpriteRenderer = Instantiate(spriteRenderer);
        inRangeSpriteRenderer.sprite = inRangeSprite;
        inRangeSpriteRenderer.transform.parent = spriteRenderer.transform.parent;
        inRangeSpriteRenderer.transform.localPosition = Vector3.zero;
        inRangeSpriteRenderer.DOFade(0f, 0f);

        fadeSequence = DOTween.Sequence();
        fadeSequence.Insert(0f, spriteRenderer.DOFade(0f, IN_RANGE_TWEEN_DURATION).SetEase(inRangeTweenEase));
        fadeSequence.Insert(0f, inRangeSpriteRenderer.DOFade(1f, IN_RANGE_TWEEN_DURATION).SetEase(inRangeTweenEase));
        fadeSequence.Insert(IN_RANGE_TWEEN_DURATION, spriteRenderer.DOFade(1f, IN_RANGE_TWEEN_DURATION).SetEase(inRangeTweenEase));
        fadeSequence.Insert(IN_RANGE_TWEEN_DURATION, inRangeSpriteRenderer.DOFade(0f, IN_RANGE_TWEEN_DURATION).SetEase(inRangeTweenEase));
        fadeSequence.SetLoops(-1);
        fadeSequence.Pause();
    }

    private void OnEnable()
    {
        interactable.InteractInRangeStateChangedEvent += OnInRangeStateChanged;
    }

    private void OnDisable()
    {
        interactable.InteractInRangeStateChangedEvent -= OnInRangeStateChanged;
    }

    private void OnInRangeStateChanged(bool newState)
    {
        if(newState)
        {
            fadeSequence.Restart();
        }
        else
        {
            fadeSequence.Pause();

            //Reset alpha's
            spriteRenderer.DOFade(1f, 0f);
            inRangeSpriteRenderer.DOFade(0f, 0f);
        }
    }
}