﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioClipEntry
{
    public string ClipName;
    public AudioClip Clip;
    [Range(0f, 1f)]
    public float Volume;
}

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance 
    {
        get
        {
            if(instance == null) { instance = FindObjectOfType<AudioManager>(); }
            return instance;
        }
    }
    private static AudioManager instance;

    [SerializeField] private AudioClipEntry[] entries;

    private List<AudioSource> audioPlayers = new List<AudioSource>();

    public void PlaySound(string clipName)
    {
        AudioSource source = FindAvailableSource();
        AudioClipEntry entry = FindClip(clipName);

        if(entry == null) { Debug.LogWarning($" not find clip with name {clipName}"); return; }

        source.clip = entry.Clip;
        source.volume = entry.Volume;
        source.Play();
    }

    private AudioClipEntry FindClip(string clipName)
    {
        for (int i = 0; i < entries.Length; i++)
        {
            if (entries[i].ClipName.Equals(clipName))
                return entries[i];
        }
        return null;
    }

    private AudioSource FindAvailableSource()
    {
        for (int i = 0; i < audioPlayers.Count; i++)
        {
            AudioSource source = audioPlayers[i];
            if (!source.isPlaying) return source;
        }
        return AddSource();
    }

    private AudioSource AddSource()
    {
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
        source.loop = false;

        audioPlayers.Add(source);

        return source;
    }
}
