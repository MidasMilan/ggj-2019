﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepAudio : MonoBehaviour
{
    [SerializeField] private float soundInterval;
    [SerializeField] private float minimumMagnitude;

    private MoveableObject movement;
    private float previousSoundPlayTimestamp;

    private void Start()
    {
        movement = GetComponent<MoveableObject>();
    }

    private void Update()
    {
        if(movement.Velocity.magnitude > minimumMagnitude)
        {
            if(Time.realtimeSinceStartup >= previousSoundPlayTimestamp + soundInterval)
            {
                PlaySound();
            }
        }
    }

    private void PlaySound()
    {
        previousSoundPlayTimestamp = Time.realtimeSinceStartup;
        AudioManager.Instance.PlaySound("Footstep");        
    }
}