﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomVisibilityController : MonoBehaviour
{
    [SerializeField] private List<GameObject> hideRoomSprites;
    public enum RoomVisibilityState { Undiscovered, FogOfWar, FullVisibility }
    RoomVisibilityState currentState = RoomVisibilityState.Undiscovered;

    public delegate void RoomVisibilityHandler(RoomVisibilityController roomController);
    public delegate void PlayerEnemySameRoomEventHandler(GameObject enemy, bool isPlayerInRoom);
    public event RoomVisibilityHandler RoomDiscoveredEvent;
    public event RoomVisibilityHandler AddFogToRoomEvent;
    public event RoomVisibilityHandler RemoveFogFromRoomEvent;
    public event PlayerEnemySameRoomEventHandler PlayerAndEnemyInSameRoomEvent;

    public RoomVisibilityState GetRoomVisibilityState() { return currentState; }

    private List<GameObject> peopleInRoom = new List<GameObject>();
    private bool containsPlayer;

    private void OnEnable()
    {
        RoomDiscoveredEvent += OnDiscover;
        AddFogToRoomEvent += OnAddFog;
        RemoveFogFromRoomEvent += OnRemoveFog;
    }

    private void OnDisable()
    {
        RoomDiscoveredEvent -= OnDiscover;
        AddFogToRoomEvent -= OnAddFog;
        RemoveFogFromRoomEvent -= OnRemoveFog;
    }

    void OnRemoveFog(RoomVisibilityController roomController)
    {
        if (currentState == RoomVisibilityState.Undiscovered)
        {
            OnDiscover(this);
        }
        foreach (GameObject sprite in hideRoomSprites) sprite.SetActive(false);
        currentState = RoomVisibilityState.FullVisibility;
    }

    void OnDiscover(RoomVisibilityController roomController)
    {
        foreach (GameObject sprite in hideRoomSprites) sprite.GetComponent<SpriteRenderer>().color = new Color(0.0f, 0.0f, 0.0f, 0.5f);
    }

    void OnAddFog(RoomVisibilityController roomController)
    {
        foreach (GameObject sprite in hideRoomSprites) sprite.SetActive(true);
        currentState = RoomVisibilityState.FogOfWar;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            RemoveFogFromRoomEvent(this);
            containsPlayer = true;
            FirePlayerEnemySameRoomEvents();
        }
        if (collision.CompareTag("Enemy"))
        {
            collision.GetComponent<FogOfWarHider>().SubscribeRoom(this);
            peopleInRoom.Add(collision.gameObject);
            FirePlayerEnemySameRoomEvents();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            AddFogToRoomEvent(this);
            containsPlayer = false;
            FirePlayerEnemySameRoomEvents();
        }
        if (collision.CompareTag("Enemy"))
        {
            collision.GetComponent<FogOfWarHider>().UnsubscribeRoom(this);
            peopleInRoom.Remove(collision.gameObject);
            FirePlayerEnemySameRoomEvents();
        }
    }

    private void FirePlayerEnemySameRoomEvents()
    {
        for (int i = 0; i < peopleInRoom.Count; i++)
        {
            PlayerAndEnemyInSameRoomEvent?.Invoke(peopleInRoom[i], containsPlayer);
        }
    }
}