﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class FogOfWarHider : MonoBehaviour
{
    public delegate void RoomChangedEventHandler(RoomVisibilityController newRoom);
    public event RoomChangedEventHandler RoomChangedEvent;

    RoomVisibilityController lastRoomController;

    public void SubscribeRoom(RoomVisibilityController roomController)
    {
        roomController.RemoveFogFromRoomEvent += OnAppear;
        roomController.AddFogToRoomEvent += OnHide;
        lastRoomController = roomController;

        RoomChangedEvent?.Invoke(roomController);
    }

    public void UnsubscribeRoom(RoomVisibilityController roomController)
    {
        roomController.RemoveFogFromRoomEvent -= OnAppear;
        roomController.AddFogToRoomEvent -= OnHide;
        OnHide(lastRoomController);

        RoomChangedEvent?.Invoke(roomController);
    }

    private void OnDisable()
    {
        if (lastRoomController != null)
        {
            lastRoomController.RemoveFogFromRoomEvent -= OnAppear;
            lastRoomController.AddFogToRoomEvent -= OnHide;
        }
    }

    private void OnHide(RoomVisibilityController roomController)
    {
        if (lastRoomController.GetRoomVisibilityState() != RoomVisibilityController.RoomVisibilityState.FullVisibility)
        {
            GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    private void OnAppear(RoomVisibilityController roomController)
    {
        GetComponent<SpriteRenderer>().enabled = true;
    }
}
