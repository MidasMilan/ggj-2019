﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFieldOfView : MonoBehaviour
{
    public delegate void PlayerSpottedEventHandler(GameObject enemy);
    public event PlayerSpottedEventHandler PlayerSpottedEvent;

    [SerializeField] private float viewingAngle;
    [SerializeField] private float viewingRange;

    private Transform player;
    private SoundListener soundListener;
    private WaypointAIController AIController;
    private bool isAwake;
    private bool shouldCheck;
    private RoomVisibilityController currentRoom;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        AIController = GetComponent<WaypointAIController>();
    }

    private void OnEnable()
    {
        if(soundListener == null) soundListener = GetComponent<SoundListener>();
        soundListener.ListenerAwokenEvent += OnListenerAwoken;
        GetComponent<FogOfWarHider>().RoomChangedEvent += OnRoomChanged;
    }

    private void OnDisable()
    {
        soundListener.ListenerAwokenEvent -= OnListenerAwoken;
        GetComponent<FogOfWarHider>().RoomChangedEvent -= OnRoomChanged;
    }

    private void OnListenerAwoken(SoundListener listener)
    {
        isAwake = true;
    }

    private void Update()
    {
        if(!isAwake || !shouldCheck) { return; }

        if(CanSeeTarget(player))
        {
            PlayerSpottedEvent?.Invoke(gameObject);
            enabled = false;

            //TODO: game over
            Minigame.CurrentMinigame?.EndMinigame();
            FindObjectOfType<ScoreCounter>().TallyBriefingPoints();
            player.GetComponent<PlayerMovement>().enabled = false;

            TimeController.PauseTimeEvent?.Invoke();
        }
    }

    private void OnRoomChanged(RoomVisibilityController newRoom)
    {
        if(currentRoom != null)
        {
            currentRoom.PlayerAndEnemyInSameRoomEvent -= ToggleChecking;
        }

        currentRoom = newRoom;
        currentRoom.PlayerAndEnemyInSameRoomEvent += ToggleChecking;
    }

    private void ToggleChecking(GameObject enemy, bool isPlayerInRoom)
    {
        if (enemy != gameObject) { return; }
        shouldCheck = isPlayerInRoom;
    }

    private bool CanSeeTarget(Transform target)
    {
        float angle = Vector3.SignedAngle(AIController.Velocity, target.position - transform.position, Vector3.up);
        float distance = Vector3.Distance(transform.position, target.position);

        return ((Mathf.Abs(angle)) <= viewingAngle && distance <= viewingRange);
    }
}
