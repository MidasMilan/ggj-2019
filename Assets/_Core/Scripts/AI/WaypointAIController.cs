﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SoundEmitter))]
public class WaypointAIController : MoveableObject
{
    [SerializeField] Transform[] waypoints;
    int currentWaypointTarget;

    [SerializeField] float speed = 0.3f;
    [SerializeField] float footstepInterval = 0.33f;
    float footstepCounter;

    bool patrolling;
    bool lastWaypoint;
    EnemyFieldOfView fieldOfView;
    
    public delegate void AISystemHandler(WaypointAIController agentController);
    public event AISystemHandler StartPatrolEvent;
    public event AISystemHandler EndPatrolEvent;

    private void Awake()
    {
        fieldOfView = GetComponent<EnemyFieldOfView>();
    }

    void OnEnable()
    {
        GetComponent<SoundListener>().ListenerAwokenEvent += OnStartPatrol;
        EndPatrolEvent += OnPathComplete;
        fieldOfView.PlayerSpottedEvent += OnPlayerSpotted;
    }

    void OnDisable()
    {
        GetComponent<SoundListener>().ListenerAwokenEvent -= OnStartPatrol;
        EndPatrolEvent -= OnPathComplete;
        fieldOfView.PlayerSpottedEvent -= OnPlayerSpotted;
    }

    private void OnStartPatrol(SoundListener listener)
    {
        currentWaypointTarget = 0;
        footstepCounter = 0.0f;
        patrolling = true;
    }

    void Update()
    {
        if (patrolling)
        {
            footstepCounter -= Time.deltaTime;
            if (footstepCounter <= 0.0f)
            {
                footstepCounter = footstepInterval;
                GetComponent<SoundEmitter>().EmitSound();
            }

            Vector3 distanceToTarget = waypoints[currentWaypointTarget].position - gameObject.transform.position;
            Vector3 walkDistance = distanceToTarget.normalized * speed * Time.deltaTime;

            if (distanceToTarget.magnitude < walkDistance.magnitude)
            {
                transform.Translate(distanceToTarget);
                Velocity = distanceToTarget;
                GoToNextWaypoint();
            }
            else
            {
                transform.Translate(walkDistance);
                Velocity = walkDistance;
            }
        }
    }

    void OnPathComplete(WaypointAIController agentController)
    {
        patrolling = false;
        Debug.Log("Agent: " + agentController.gameObject.name + "Finished his path");
    }

    private void OnPlayerSpotted(GameObject enemy)
    {
        patrolling = false;
        fieldOfView.PlayerSpottedEvent -= OnPlayerSpotted;
    }

    void GoToNextWaypoint()
    {
        currentWaypointTarget++;
        if (currentWaypointTarget == waypoints.Length) EndPatrolEvent(this);
    }
}