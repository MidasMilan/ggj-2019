﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeController : MonoBehaviour
{
    public delegate void TimeSystemHandler();
    public static event TimeSystemHandler TimeUpEvent;
    public static event TimeSystemHandler StartTimeEvent;
    public static TimeSystemHandler PauseTimeEvent;

    [SerializeField] private float dayLength;
    [SerializeField] private Text timeUICounter;
    private float timeLeft;
    private bool timeRunning;

    void OnEnable()
    {
        StartTimeEvent += OnTimeStart;
        PauseTimeEvent += OnTimePause;
        TimeUpEvent += OnTimeUp;
        TimeUpEvent += OnTimePause;
    }

    void OnDisable()
    {
        StartTimeEvent -= OnTimeStart;
        PauseTimeEvent -= OnTimePause;
        TimeUpEvent -= OnTimeUp;
        TimeUpEvent -= OnTimePause;
    }

    public void OnTimeUp()
    {
        timeUICounter.text = "Time's up, BITCH";
    }

    public void OnTimeStart()
    {
        timeRunning = true;
    }

    public void OnTimePause()
    {
        timeRunning = false;
    }

    void Start()
    {
        timeLeft = dayLength;
        StartTimeEvent();
    }

    void Update()
    {
        if (timeRunning)
        {
            timeLeft -= Time.deltaTime;
            UpdateUITime();

            if (timeLeft < 0.0f)
            {
                timeLeft = 0.0f;
                TimeUpEvent();
            }
        }
    }

    void UpdateUITime()
    {
        float secondsLeft = timeLeft % 60;
        string secondsOutput = Mathf.Floor(secondsLeft).ToString();
        if (secondsLeft < 10) secondsOutput = "0" + secondsOutput;

        float minutesLeft = Mathf.Floor(timeLeft / 60);

        float millisecondsLeft = Mathf.Floor((secondsLeft - Mathf.Floor(secondsLeft)) * 1000);
        string millisecondsOutput = millisecondsLeft.ToString();
        if (millisecondsLeft < 100 && millisecondsLeft >= 10) millisecondsOutput = "0" + millisecondsLeft.ToString();
        else if (millisecondsLeft < 10) millisecondsOutput = "00" + millisecondsLeft.ToString();

        timeUICounter.text = minutesLeft.ToString() + ":" + secondsOutput + ":" + millisecondsOutput;
    }
}
