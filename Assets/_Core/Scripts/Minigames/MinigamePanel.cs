﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MinigamePanel : MonoBehaviour
{
    private RectTransform rectTransform;
    private float width;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();

        width = rectTransform.sizeDelta.x;
        rectTransform.anchoredPosition = new Vector3(width, 0f);
    }

    public void Toggle(bool newState)
    {
        rectTransform.DOAnchorPosX(newState ? 0 : width, 1f);
    }
}
