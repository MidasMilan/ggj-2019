﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    public int scoreValue;
    public Text score;

    public Text TallyScore;

    private List<PenaltyData> penalties;

    private void OnEnable()
    {
        TimeController.TimeUpEvent += TallyBriefingPoints;
        Minigame.MinigameEndedEvent += AddUpPoints;
    }

    private void OnDisable()
    {
        TimeController.TimeUpEvent -= TallyBriefingPoints;
        Minigame.MinigameEndedEvent -= AddUpPoints;
    }

    void Start()
    {
        score = GetComponent<Text>();
        penalties = new List<PenaltyData>();
        TallyScore.enabled = false;
    }

    void Update()
    {
        score.text = "Score: " + scoreValue;
    }

    public void AddUpPoints(int points)
    {
        scoreValue += points;
    }

    void SubtractPoints(int points)
    {
        scoreValue -= points;
    }

    public void AddPointPenalty(PenaltyData data)
    {
        penalties.Add(data);
    }

    public void TallyBriefingPoints()
    {
        TallyScore.enabled = true;

        foreach (PenaltyData data in penalties)
        {
            TallyScore.text += "\n" + data.name + ": -" + data.points;
            SubtractPoints(data.points);
        }
    }
}
