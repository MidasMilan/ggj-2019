﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;

public class Minigame : MonoBehaviour
{
    public static Minigame CurrentMinigame;

    public delegate void MinigameStateChangedEventHandler();
    public event MinigameStateChangedEventHandler MinigameStartedEvent;
    public delegate void MinigameEndedEventHandler(int PointsEarned);
    public static event MinigameEndedEventHandler MinigameEndedEvent;

    public bool IsPlayed { get; private set; }
    public float NormalizedProgress { get { return (float)succesfulTicks / totalTicksNeeded; } }

    [Header("References")]
    [SerializeField] protected Interactable interactable;
    [SerializeField] protected SoundEmitter soundEmitter;
    [Header("Score settings")]
    [SerializeField] private int baseScoreValue;
    [SerializeField] private int bonuScoreValue;
    [SerializeField] private float bonusPenaltyInterval;
    [SerializeField] private int bonusPenaltyAmount;
    [SerializeField] private int totalTicksNeeded; //for completion

    private GameObject player;
    private MinigamePanel panel;
    private float minigameStartTimeStamp;
    private int succesfulTicks;

    protected virtual void OnEnable()
    {
        if(interactable != null)
        {
            interactable.InteractEvent += StartMinigame;
        }
    }

    protected virtual void OnDisable()
    {
        if (interactable != null)
        {
            interactable.InteractEvent -= StartMinigame;
        }
    }

    protected virtual void Start()
    {
        panel = FindObjectOfType<MinigamePanel>();
    }

    protected void SuccesfullTick()
    {
        succesfulTicks++;

        if(succesfulTicks >= totalTicksNeeded)
        {
            EndMinigame();
        }
    }

    private void StartMinigame(PlayerInteracting player, Interactable interactable)
    {
        this.player = player.gameObject;

        //Disable interactable and player movement
        interactable.enabled = false;
        player.GetComponent<PlayerMovement>().enabled = false;
        player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        player.transform.position = interactable.transform.position;

        Vector3 minigameCamPos = transform.position;
        minigameCamPos.z = -10;
        GameObject.FindGameObjectWithTag("MinigameCamera").transform.position = minigameCamPos;

        CurrentMinigame = this;

        StartCoroutine(MinigameSequence());
    }

    private IEnumerator MinigameSequence()
    {
        AnimateCameras(true);

        yield return new WaitForSeconds(1f);

        MinigameStartedEvent?.Invoke();
        minigameStartTimeStamp = Time.realtimeSinceStartup;
    }

    public void EndMinigame(bool forceQuit = false)
    {
        player.GetComponent<PlayerMovement>().enabled = true;
        player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        IsPlayed = true;

        AnimateCameras(false);
        
        float totalTime = Time.realtimeSinceStartup - minigameStartTimeStamp;
        bonuScoreValue -= (int)(Mathf.Floor(totalTime / bonusPenaltyInterval) * bonusPenaltyAmount);
        if (bonuScoreValue < 0) bonuScoreValue = 0;
        int totalScore = baseScoreValue + bonuScoreValue;

        if (!forceQuit)
            MinigameEndedEvent?.Invoke(totalScore);

        CurrentMinigame = null;
    }

    private void AnimateCameras(bool minigameState)
    {
        CinemachineVirtualCamera minigameCam = GameObject.FindGameObjectWithTag("MinigamePlayerCamera").GetComponent<CinemachineVirtualCamera>();
        CinemachineVirtualCamera normalCam = GameObject.FindGameObjectWithTag("NormalPlayerCamera").GetComponent<CinemachineVirtualCamera>();

        minigameCam.enabled = minigameState;
        normalCam.enabled = !minigameState;

        panel.Toggle(minigameState);
    }
}