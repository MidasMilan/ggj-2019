﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenaltyData
{
    public string name;
    public int points;

    public PenaltyData(string name, int points) 
    {
        this.name = name;
        this.points = points;
    }
}
