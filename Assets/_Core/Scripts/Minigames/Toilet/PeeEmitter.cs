﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PeeEmitter : MonoBehaviour
{
    public delegate void PeeParticleLandedEventHandler(bool InBowl, bool inHole);
    public event PeeParticleLandedEventHandler PeeParticleLandedEvent;

    [Header("References")]
    [SerializeField] private GameObject peePrefab;
    [SerializeField] private Transform peeTargetPoint;
    [Header("Config")]
    [SerializeField] private float emitInterval;
    [SerializeField] private float peeTravelTime;
    [SerializeField] private float aimMoveSpeed;
    [SerializeField] private float drunknessMovement;
    [SerializeField] private float drunkenssInterval;
    [Header("Colliders")]
    [SerializeField] private CircleCollider2D bowlCollider;
    [SerializeField] private CircleCollider2D bowlHoleCollider;

    private bool isEmitting;
    private float lastEmitTimestamp;
    private Vector2 aimTarget;

    public void ToggleEmitter(bool newState)
    {
        isEmitting = newState;
        if (newState) { MoveTargetRandomly(); }
    }

    private void Update()
    {
        if (!isEmitting) { return; }   
        if(Time.realtimeSinceStartup >= lastEmitTimestamp + emitInterval)
        {
            Emit();
        }

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        peeTargetPoint.position += (Vector3)input * aimMoveSpeed * Time.deltaTime;
    }

    private void MoveTargetRandomly()
    {
        Vector3 randomness = new Vector3(Random.Range(-drunknessMovement, drunknessMovement),
                                         Random.Range(-drunknessMovement, drunknessMovement));
        peeTargetPoint.DOBlendableMoveBy(randomness, drunkenssInterval).OnComplete(delegate() 
        {
            if (isEmitting) { MoveTargetRandomly(); }
        });
    }

    private void Emit()
    {
        GameObject peeParticle = Instantiate(peePrefab);
        peeParticle.transform.position = transform.position;
        peeParticle.transform.DOMove(peeTargetPoint.position, peeTravelTime).OnComplete(delegate()
        {
            CheckPeeParticleCollision(peeParticle.transform.position);
            Destroy(peeParticle);
        });
        peeParticle.transform.DOScale(0.25f, peeTravelTime);

        lastEmitTimestamp = Time.realtimeSinceStartup;
    }
    
    private void CheckPeeParticleCollision(Vector3 position)
    {
        bool inBowl = bowlCollider.bounds.Contains(position);
        bool inHole = bowlHoleCollider.bounds.Contains(position);

        PeeParticleLandedEvent?.Invoke(inBowl, inHole);
    }
}