﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameToilet : Minigame
{
    [Header("Minigame specific")]
    [SerializeField] private PeeEmitter peeEmitter;

    protected override void OnEnable()
    {
        base.OnEnable();
        MinigameStartedEvent += OnMinigameStarted;
        MinigameEndedEvent += OnMinigameEnded;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        MinigameStartedEvent -= OnMinigameStarted;
        MinigameEndedEvent -= OnMinigameEnded;
    }

    private void OnMinigameStarted()
    {
        peeEmitter.ToggleEmitter(true);
        peeEmitter.PeeParticleLandedEvent += OnPeeParticleLanded;
    }

    private void OnMinigameEnded(int score)
    {
        peeEmitter.ToggleEmitter(false);
        peeEmitter.PeeParticleLandedEvent -= OnPeeParticleLanded;
    }

    private void OnPeeParticleLanded(bool InBowl, bool inHole)
    {
        if(InBowl) { SuccesfullTick(); }
        if(!InBowl || inHole) { soundEmitter?.EmitSound(interactable.transform.position); }        
    }
}
