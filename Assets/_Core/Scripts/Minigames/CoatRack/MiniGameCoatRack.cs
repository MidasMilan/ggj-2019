﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameCoatRack : Minigame
{
    [Header("Minigame specific")]
    [SerializeField] private HangTarget hangTarget;
    [Range(0.0f, 10.0f)]
    [SerializeField] private float missHangerVolume;
    [Range(0.0f, 10.0f)]
    [SerializeField] private float missRackVolume;

    protected override void OnEnable()
    {
        base.OnEnable();
        MinigameStartedEvent += OnMinigameStarted;
        MinigameEndedEvent += OnMinigameEnded;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        MinigameStartedEvent -= OnMinigameStarted;
        MinigameEndedEvent -= OnMinigameEnded;
    }

    private void OnMinigameStarted()
    {
        hangTarget.ToggleAim(true);
        hangTarget.HangAttemptEvent += OnHangAttempt;
    }

    private void OnMinigameEnded(int score)
    {
        hangTarget.ToggleAim(false);
        hangTarget.HangAttemptEvent -= OnHangAttempt;
    }

    private void OnHangAttempt(bool inCoatHanger, bool inCoatRack)
    {
        if (inCoatHanger)
        {
            SuccesfullTick();
        }
        else if (!inCoatHanger && inCoatRack)
        {
            soundEmitter.volume = missHangerVolume;
            soundEmitter?.EmitSound(interactable.transform.position);
        }
        else if (!inCoatHanger && !inCoatRack)
        {
            soundEmitter.volume = missRackVolume;
            soundEmitter?.EmitSound(interactable.transform.position);
        }
    }
}
