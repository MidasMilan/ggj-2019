﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HangTarget : MonoBehaviour
{
    public delegate void HangAttemptEventHandler(bool inCoatHanger, bool inCoatRack);
    public event HangAttemptEventHandler HangAttemptEvent;

    [Header("References")]
    [SerializeField] private Transform aimTargetPoint;
    [Header("Config")]
    [SerializeField] private float aimMoveSpeed;
    [SerializeField] private float drunknessMovement;
    [SerializeField] private float drunkenssInterval;
    [Header("Colliders")]
    [SerializeField] private Collider2D coatHangerCollider;
    [SerializeField] private Collider2D coatRackCollider;

    private bool isAiming;
    private Vector2 aimTarget;

    public void ToggleAim(bool newState)
    {
        isAiming = newState;
        if (newState) { MoveTargetRandomly(); }
    }

    private void Update()
    {
        if (!isAiming) { return; }
        if (Input.GetKeyDown(KeyCode.Space)) CheckHangAttempt(aimTargetPoint.position);

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        aimTargetPoint.position += (Vector3)input * aimMoveSpeed * Time.deltaTime;
    }

    private void MoveTargetRandomly()
    {
        Vector3 randomness = new Vector3(Random.Range(-drunknessMovement, drunknessMovement),
                                         Random.Range(-drunknessMovement, drunknessMovement));
        aimTargetPoint.DOBlendableMoveBy(randomness, drunkenssInterval).OnComplete(delegate ()
        {
            if (isAiming) { MoveTargetRandomly(); }
        });
    }

    private void CheckHangAttempt(Vector3 position)
    {
        bool inCoatHanger = coatHangerCollider.bounds.Contains(position);
        bool inCoatRack = coatHangerCollider.bounds.Contains(position);

        HangAttemptEvent?.Invoke(inCoatHanger, inCoatRack);
    }
}
