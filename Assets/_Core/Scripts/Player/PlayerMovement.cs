﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableObject : MonoBehaviour
{
    public Vector2 Velocity { get; protected set; }
}

public class PlayerMovement : MoveableObject
{
    [SerializeField] private float walkSpeed = 0.25f;
    //[SerializeField] private float runSpeed = 2.0f;
    [SerializeField] private float walkAcceleration = 1.0f;
    //[SerializeField] private float runAcceleration = 0.2f;
    [SerializeField] private float deceleration = 2.0f;
    private float horizontalSpeed, verticalSpeed, maxSpeed;
    private float currentAcceleration;

    [SerializeField] float footstepInterval = 1f;
    private float footstepCounter;

    private Vector2 movementInput;

    private void Start()
    {
        currentAcceleration = walkAcceleration;
        maxSpeed = walkSpeed;
        footstepCounter = footstepInterval;
    }

    void Update()
    {
        currentAcceleration = walkAcceleration;
        maxSpeed = walkSpeed;

        movementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (movementInput.x != 0f || movementInput.y != 0f)
        {
            footstepCounter -= Time.deltaTime;
            if (footstepCounter <= 0.0f)
            {
                footstepCounter = footstepInterval;
                GetComponent<SoundEmitter>()?.EmitSound();
            }
        }

        //Horizontal speed calculation
        if (movementInput.x < 0.0f)
        {
            if (horizontalSpeed > -maxSpeed) horizontalSpeed -= currentAcceleration * Time.deltaTime;
            else horizontalSpeed = -maxSpeed;
        }
        else if (movementInput.x > 0.0f)
        {
            if (horizontalSpeed < maxSpeed) horizontalSpeed += currentAcceleration * Time.deltaTime;
            else horizontalSpeed = maxSpeed;
        }
        else
        {
            if (horizontalSpeed > deceleration * Time.deltaTime) horizontalSpeed -= deceleration * Time.deltaTime;
            else if (horizontalSpeed < -deceleration * Time.deltaTime) horizontalSpeed += deceleration * Time.deltaTime;
            else horizontalSpeed = 0;
        }

        //Vertical speed calculation
        if (movementInput.y < 0.0f)
        {
            if (verticalSpeed > -maxSpeed) verticalSpeed -= currentAcceleration * Time.deltaTime;
            else verticalSpeed = -maxSpeed;
        }
        else if (movementInput.y > 0.0f)
        {
            if (verticalSpeed < maxSpeed) verticalSpeed += currentAcceleration * Time.deltaTime;
            else verticalSpeed = maxSpeed;
        }
        else
        {
            if (verticalSpeed > deceleration * Time.deltaTime) verticalSpeed -= deceleration * Time.deltaTime;
            else if (verticalSpeed < -deceleration * Time.deltaTime) verticalSpeed += deceleration * Time.deltaTime;
            else verticalSpeed = 0;
        }
        
        Vector3 delta = new Vector3(horizontalSpeed, verticalSpeed);
        GetComponent<Rigidbody2D>().MovePosition(new Vector2(transform.position.x + horizontalSpeed
                                                            , transform.position.y + verticalSpeed));
        Velocity = delta;
    }
}
